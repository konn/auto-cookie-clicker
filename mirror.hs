{-# LANGUAGE OverloadedStrings #-}
module Main where
import Network.Wai.Application.Static (defaultFileServerSettings)
import Network.Wai.Application.Static (ssRedirectToIndex, staticApp)
import Network.Wai.Handler.Warp       (run)


main :: IO ()
main = run 8787 $ staticApp $
       (defaultFileServerSettings  "src") { ssRedirectToIndex = True
                                          }

