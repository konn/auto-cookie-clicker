var casper = require("casper").create();
var viewport = {width: 800, height: 500};
var process = require("child_process");
var execFile  = process.spawn;
var shoppingInterval = 1000*60*5;

casper.start("http://127.0.0.1:8787/", function() {
    this.page.viewportSize = viewport;
    this.page.zoomFactor = 0.65;
});

casper.waitFor(
    function(){
        var game = this.evaluate(function(){return JSON.stringify(Game)});
        return game;
    },
    function then() {
        console.log("Game generated!");
    })

casper.waitForSelector("#bigCookie", function() {
    console.log("cookie came out!");
    var save = casper.evaluate(function(){return Game.WriteSave(1);});
    setInterval(function() {
	casper.click("#bigCookie");
	if (casper.visible("#goldenCookie")) {
	    casper.click("#goldenCookie");
	}
        // Buy The Elder Pledge if absent
        bought = casper.evaluate(function(){
            if (Game.Upgrades["Elder Pledge"].bought < 1) {
                Game.Upgrades["Elder Pledge"].buy();
                return true;
            } else {
                return false;
            }
        });
        if (bought) { console.log("baught Elder Pledge"); }
    }, 1000*5);
    loadData();
});

casper.then(function() {
    setInterval(function() {
	casper.capture("./cookie-status.jpg",
		       {top: 15, left: 0, height: 485, width:790},
		       {format: "jpg", quality: 100});
    }, 1000*60);
    checkForUpdates();
    setTimeout(strategy, 4*1000);
    setInterval(logStatus, 1000*30);
});

function checkForUpdates() {
    setInterval(function(){
	if (casper.visible("#alert")) {
	    saveData();
	    casper.reload();
	    loadData();
	}
    }, 3600*1000);
}

function logStatus() {
    var currentStatus = casper.evaluate(function(){
	var stat = {};
	stat['cookies'] = Game.cookies;
	stat['cps'] = Game.cookiesPs;
	stat['objects'] = {};
	for (var i in Game.Objects) {
	    var me = Game.Objects[i]
	    stat.objects[i] = {name: me.id, amount: me.amount, price: me.price,
			       total: me.totalCookies, cps: me.storedTotalCps}
	}
	stat['goldenCookies'] = Game.goldenClicks;
	return stat;
    });
    try {
        var page = require('webpage').create();
        page.open('http://localhost:24225/cookie.status', 'post', 'json='+JSON.stringify(currentStatus),
                  function(s) {
                      if (s !== 'success') {
                          console.log('Log Failed!: ' + st);
                      } else {
                          console.log('log finished');
                          page.close();
                      }
                  });
    } catch (e) {
        console.log('logging failed!: ' + e);
    }
    saveData();
};

function saveData() {
    execFile("redis-cli", ["set", "cookie-var", String(casper.evaluate(function(){return Game.WriteSave(1);}))]);
}

function loadData() {
    execFile("redis-cli", ["get", "cookie-var"], null, function(err, stdout, stderr){
	casper.evaluate(function (save){Game.LoadSave(save);},
                        stdout.replace(/^\s+|\s+$/g, ''));
    });
}

function strategy() {
    var oldCookieAmount = casper.evaluate(function() {
        return Game.cookies;
    });
    var target = casper.evaluate(function() {
	var objs = [];
	var current = Game.cookies;
	for (var i in Game.Objects) {
	    var me = Game.Objects[i];
	    if (me.price <= Game.cookies / 2) {
		objs.push({object: me, cpspc: me.storedTotalCps / Game.cookies})
	    };
	}
	if (objs.length > 0) {
	    var target = objs.reduce(function(acc, cur, ind, arr){
		console.log([acc, cur]);
		if (acc.cpspc < cur.cpspc) {
		    return cur;
		} else if (acc.object.price <= cur.object.price) {
		    return acc;
		} else {
		    return cur;
		}
	    }).object;
	    return target;
	} 
    });
    if (target)	{
	casper.evaluate(function (target) {Game.ObjectsById[target.id].buy()}, target);
	casper.echo("baught " + target.name);
    }
    var curCps = casper.evaluate(function(){return Game.cookiesPs});
    var nowCookieAmount = casper.evaluate(function() {
        return Game.cookies;
    });
    var wait = Math.max(0, 1.5 * oldCookieAmount - nowCookieAmount) / curCps;
    console.log('Wait for ' + wait/60 + ' mins for next shopping');
    setTimeout(strategy, wait*1000);
}


var server = require("webserver").create({viewportSize: viewport});
server.listen(9898, function(req, rsp) {
    if (req.url == "/") {
	switch (req.method) {
	case "GET":
	    rsp.responseCode = 200;
	    rsp.write(casper.evaluate(function(){return Game.WriteSave(1);}));
	    break;
	case "PUT":
	    rsp.responseCode = 200;
	    console.log("before state: " + casper.evaluate(function(){return Game.cookies;}));
	    casper.evaluate(function(save){return Game.LoadSave(save);}, req.post);
	    console.log("new state: " + casper.evaluate(function(){return Game.cookies;}));
	default:
	    rsp.responseCode = 404;
	    rsp.write("NotFound");
	}
    } else {
	rsp.responseCode = 404;
	rsp.write("Not Found");
    }
    rsp.closeGracefully();
});

casper.run(function(){
    this.echo(this.evaluate(function () {return [Game.cookies, Game.goldenClicks]}));
});
