{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wall #-}
module Main where
import           Control.Monad           (unless, void, when)
import           Control.Monad.Trans     (liftIO)
import           Data.Digest.Pure.MD5    (md5)
import           Data.Maybe              (fromJust)
import qualified Data.Text.Lazy.Encoding as T
import           Database.Redis.Simple   (Key)
import           Network.HTTP.Conduit    (Request (..))
import           Network.HTTP.Conduit    (RequestBody (RequestBodyLBS), httpLbs)
import           Network.HTTP.Conduit    (newManager, parseUrl, simpleHttp)
import           Network.HTTP.Conduit    (tlsManagerSettings)
import           Network.HTTP.Types      (StdMethod (OPTIONS))
import           Web.Scotty              (ActionM, addHeader, addroute, body)
import           Web.Scotty              (get, param, put, scotty, text)

cookieKey :: Key
cookieKey = "cookie-var"

passHash :: String
passHash = "cf4c1f8858b54216e34b7b5758129ced"

setHeaders :: ActionM ()
setHeaders = do
  addHeader  "Access-Control-Allow-Origin" "*"
  addHeader  "Access-Control-Allow-Methods" "GET,PUT"
  addHeader  "Access-Control-Allow-Headers" "Content-Type"

withSigned :: ActionM () -> ActionM ()
withSigned act = do
  setHeaders
  h <- md5 . T.encodeUtf8 <$> param "pass"
  when (show h == passHash) act
  unless (show h == passHash) $ liftIO $ putStrLn "Auth failed!"

main :: IO ()
main = scotty 9899 $ do
    get "/" $ withSigned $ text . T.decodeUtf8 =<< simpleHttp "http://127.0.0.1:9898/"
    put "/" $ withSigned $ do
      key <- body
      man <- liftIO $ newManager tlsManagerSettings
      void $ httpLbs (fromJust $ parseUrl "http://127.0.0.1:9898/")
                                      { method = "PUT"
                                      , requestBody = RequestBodyLBS key
                                      }
                     man
      return ()
    addroute OPTIONS "/" setHeaders
