{-# LANGUAGE ExtendedDefaultRules, OverloadedStrings, QuasiQuotes #-}
{-# LANGUAGE RecordWildCards, TemplateHaskell                     #-}
{-# OPTIONS_GHC -Wall #-}
module Main where
import           Conduit
import           Control.Applicative
import           Control.Arrow
import           Control.Lens                     ((%~), _head)
import           Control.Monad
import           Data.Aeson
import           Data.Aeson.TH
import           Data.Attoparsec.ByteString.Char8 hiding (Result, takeWhile)
import qualified Data.Attoparsec.ByteString.Char8 as Atto
import qualified Data.ByteString.Char8            as BS
import           Data.Char                        (toLower)
import           Data.Conduit.Attoparsec
import qualified Data.Conduit.List                as LC
import           Data.Conduit.Zlib
import           Data.Default
import           Data.List                        hiding (stripPrefix)
import qualified Data.Map                         as M
import           Data.Maybe
import           Data.Monoid                      hiding (Product (..))
import           Data.Ord
import qualified Data.Text                        as T
import           Data.Time
import           Language.Javascript.JMacro
import           Prelude                          hiding (div, readFile)
import           System.Directory
import           System.FilePath
import           Text.Blaze.Html.Renderer.Text
import           Text.Blaze.Html5                 hiding (head, html, main, map,
                                                   time, var)
import qualified Text.Blaze.Html5                 as H5
import qualified Text.Blaze.Html5.Attributes      as Attr
import           Text.PrettyPrint.Leijen.Text     (displayT, renderPretty)
import           Web.Scotty                       hiding (body, json)

default (T.Text)

data Statistics = Statistics { stCookies       :: Double
                             , stCps           :: Double
                             , stGoldenCookies :: Integer
                             , stObjects       :: M.Map String Product
                             } deriving (Read, Show, Eq, Ord)

data Product = Product { pAmount :: Integer
                       , pCps    :: Double
                       , pName   :: Int
                       , pTotal  :: Double
                       , pPrice  :: Integer
                       } deriving (Read, Show, Eq, Ord)

deriveJSON (defaultOptions {fieldLabelModifier = (_head %~ toLower) . drop 1}) ''Product
deriveJSON (defaultOptions {fieldLabelModifier = (_head %~ toLower) . drop 2}) ''Statistics

-- instance FromJSON Statistics where
--   parseJSON (Object obj) = Statistics <$> (read <$> obj .: "cookies")
--                                       <*> (read <$> obj .: "cps")
--                                       <*> (read <$> obj .: "goldenCookies")
--                                       <*> obj .: "objects"
--   parseJSON _ = empty


-- instance FromJSON Product where
--   parseJSON (Object obj) =
--     Product <$> (read <$> obj .: "amount")
--             <*> (read <$> obj .: "cps")
--             <*> obj .: "name"
--             <*> (read <$> obj .: "total")
--             <*> (read <$> obj .: "price")
--   parseJSON _ = empty

main :: IO ()
main = scotty 8981 $ get "/" $ html . renderHtml =<< liftIO buildGraphs

fromResult :: Result a -> Maybe a
fromResult (Success n) = Just n
fromResult _ = Nothing

parseLogLine :: Parser (LocalTime, Statistics)
parseLogLine = (,) <$> timeStamp
                   <*  Atto.string "\tcookie.status\t"
                   <*> (maybe empty pure . fromResult . fromJSON =<< json)

parseLogLine' :: Parser (LocalTime, Value)
parseLogLine' = (,) <$> timeStamp
                   <*  Atto.string "\tcookie.status\t"
                   <*> json

timeStamp :: Parser LocalTime
timeStamp = parseTimeOrError True defaultTimeLocale "%Y-%m-%dT%T%z" . BS.unpack <$> Atto.takeWhile (/= '\t')

getTimeStamp :: FilePath -> (Day, Int)
getTimeStamp fp =
  case T.breakOnEnd "_" $ (!! 1) $ T.splitOn "." $ T.pack $ takeExtensions fp of
    ("", stamp) -> (parseTimeOrError True defaultTimeLocale "%Y%m%d" $ T.unpack stamp, 0)
    (stamp, no) -> (parseTimeOrError True defaultTimeLocale "%Y%m%d" $ T.unpack $ T.init stamp, read $ T.unpack no)

addPair :: (ToJExpr a, ToJExpr a1) => a -> (t -> a1) -> (LocalTime, t) -> JExpr
addPair var access (time, stat) =
   [jmacroE| `(var)`.addRow([ `(toDate time)` , `(access stat)`]) |]

toDate :: LocalTime -> JExpr
toDate time =
  let (y,m,d) = toGregorian $ localDay time
      hour = todHour $ localTimeOfDay time
      mi = todMin $ localTimeOfDay time
      sec = floor $ todSec $ localTimeOfDay time :: Int
  in [jmacroE| new Date(`(y)`, `(m - 1)` , `(d)`, `(hour)`, `(mi)`, `(sec)`) |]

data Dashboard = Dashboard { chartType    :: String
                           , chartOpts    :: JExpr
                           , chartView    :: JExpr
                           , controlType  :: String
                           , controlOpts  :: JExpr
                           , controlState :: JExpr
                           , dashboardId  :: String
                           , controlId    :: String
                           , chartId      :: String
                           , columns      :: [(String, String)]
                           } deriving (Show, Eq, Ord)

instance Default Dashboard where
    def = Dashboard { chartType    = error "not set"
                    , chartOpts    = [jmacroE| {} |]
                    , chartView    = [jmacroE| null |]
                    , controlType  = error "not set"
                    , controlOpts  = [jmacroE| {} |]
                    , controlState = [jmacroE| {} |]
                    , dashboardId  = "dashboard"
                    , controlId    = "control"
                    , chartId      = "chart"
                    , columns      = []
                    }

buildDashboard :: (ToJExpr b, ToJExpr c)
               => Dashboard -> (a -> (b, [c])) -> [a] -> JStat
buildDashboard Dashboard{..} access seeds = jVar $ \dat ->
    let cols = map (\(x, y) ->  [jmacroE| `(dat)`.addColumn(`(x)`, `(y)`) |]) columns
        rows = [jmacroE| `(dat)`.addRows(`([ toJExpr (toJExpr lab : map toJExpr rest) | x <- seeds, let (lab, rest) = access x ])`) |]
    in [jmacro| `(dat)` = new google.visualization.DataTable();
                `(cols)`;
                `(rows)`;
                var chart = new google.visualization.ChartWrapper({
                    'chartType': `(chartType)`,
                    'containerId': `(chartId)`,
                    'options': `(chartOpts)`,
                    'view': `(chartView)`
                });

                var control = new google.visualization.ControlWrapper({
                    'controlType': `(controlType)`,
                    'containerId': `(controlId)`,
                    'options': `(controlOpts)`,
                    'state' : `(controlState)`
                });

                var dashboard = new google.visualization.Dashboard(document.getElementById(`(dashboardId)`));
                dashboard.bind(control, chart);
                dashboard.draw(`(dat)`);
              |]

buildGraphs :: IO Html
buildGraphs = do
  fps <- map ("./status" </>) . sortBy (comparing getTimeStamp) . filter (isPrefixOf "cookie-status" . takeFileName) <$>
           getDirectoryContents "./status/"
  dat <- fmap concat . forM fps $ \fp ->  runResourceT $
      sourceFile fp $$ (if takeExtension fp == ".gz" then ungzip else LC.map id)
                    =$ sinkParser (parseLogLine `sepBy` char '\n')
  return $ docTypeHtml $ do
    let objs = stObjects $ snd $ head dat
        view = [jmacroE| { columns: [{ 'calc': \dataTable rowIndex ->
                                                  (new google.visualization.DateFormat({'pattern': 'HH:mm'})).formatValue(dataTable.getValue(rowIndex, 0))
                                     , 'type': 'string'}, 1]}|]

        obView = [jmacroE| { columns: [{ 'calc': \dataTable rowIndex ->
                                                  (new google.visualization.DateFormat({'pattern': 'HH:mm'})).formatValue(dataTable.getValue(rowIndex, 0))
                                     , 'type': 'string'}].concat(`(zipWith const [1 :: Int ..] $ M.keys objs)`)}|]
        chrtOpts = [jmacroE| { title: 'Cps', vAxis: {format: '#,###'}, hAxis: {format: 'HH:mm:ss'} } |]
        ctrlOpts = [jmacroE| { filterColumnIndex: 0 } |]
        dates = map fst dat
        ctrlState = [jmacroE| {range: {start: new Date (`(toDate $ maximum dates)` - 3600*1000*2), end: `(toDate $ maximum dates)`}} |]
        jsrc = [jmacro| google.load("visualization", "1", {packages:["corechart", "controls"]});
                        google.setOnLoadCallback(function() {
                              `(buildDashboard def { columns = [("date", ""), ("number", "CpS")]
                                                   , chartType = "AreaChart", controlType = "ChartRangeFilter"
                                                   , chartOpts = chrtOpts, controlOpts = ctrlOpts
                                                   , chartId = "cps-chart", controlId = "cps-control"
                                                   , dashboardId = "cps-dashboard"
                                                   , controlState = ctrlState, chartView = view
                                                   } (toDate *** (pure . stCps)) dat)`;

                              `(buildDashboard def { columns = [("date", ""), ("number", "Cookies")]
                                                   , chartType = "AreaChart", controlType = "ChartRangeFilter"
                                                   , chartOpts = chrtOpts, controlOpts = ctrlOpts
                                                   , chartId = "cookies-chart", controlId = "cookies-control"
                                                   , dashboardId = "cookies-dashboard"
                                                   , controlState = ctrlState, chartView = view
                                                   } (toDate *** (pure . stCookies)) dat)`;

                              `(buildDashboard def { columns = ("date", ""):[("number", name) | name <- M.keys objs ]
                                                   , chartType = "LineChart", controlType = "ChartRangeFilter"
                                                   , chartOpts = chrtOpts
                                                   , controlOpts = ctrlOpts
                                                   , chartId = "objects-chart", controlId = "objects-control"
                                                   , dashboardId = "objects-dashboard"
                                                   , controlState = ctrlState, chartView = obView
                                                   } (toDate *** (map (pAmount . snd) . M.toList . stObjects)) dat)`;
                       });
                      |]
    H5.head $ do
      meta ! Attr.charset "UTF-8"
      title "Cookie Statistics"
      script ! Attr.type_ "text/javascript"
             ! Attr.src "http://www.google.com/jsapi#sthash.YajrAIZv.dpuf" $ mempty
      script ! Attr.type_ "text/javascript" $ toMarkup $ displayT $ renderPretty 1.0 175 $ renderJs jsrc
    body $ do
      h1 "Cookie Status"
      h2 "Cps"
      div ! Attr.id "cps-dashboard" $ do
        div ! Attr.id "cps-chart" ! Attr.style "width: 100%; height: 240px;" $ mempty
        div ! Attr.id "cps-control" ! Attr.style "width: 100%; height: 50px;" $ mempty
      h2 "Cookies"
      div ! Attr.id "cookies-dashboard" $ do
        div ! Attr.id "cookies-chart" ! Attr.style "width: 100%; height: 240px;" $ mempty
        div ! Attr.id "cookies-control" ! Attr.style "width: 100%; height: 50px;" $ mempty
      h2 "Objects"
      div ! Attr.id "objects-dashboard" $ do
        div ! Attr.id "objects-chart" ! Attr.style "width: 100%; height: 240px;" $ mempty
        div ! Attr.id "objects-control" ! Attr.style "width: 100%; height: 50px;"$ mempty
      h2 "Screen Shot"
      p $ img ! Attr.src "/img/cookie-status.jpg"
